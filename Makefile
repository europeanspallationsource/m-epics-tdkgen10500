include $(EPICS_ENV_PATH)/module.Makefile

DOC       = README.md
MISC      = protocol/tdkGen10500.proto
STARTUPS  = startup/tdkGen10500.cmd
STARTUPS += startup/tdkGen10500sim.cmd
SRC       = src/tdkGen10500.c

USR_DEPENDENCIES = streamdevice,2.7.7

