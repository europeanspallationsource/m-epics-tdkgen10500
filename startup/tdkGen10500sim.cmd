require streamdevice, 2.7.1
require tdkgen10500, nsenaud

drvAsynIPPortConfigure("CoilsPS-01", "127.0.0.1:10001")
drvAsynIPPortConfigure("CoilsPS-02", "127.0.0.1:10002")
drvAsynIPPortConfigure("CoilsPS-03", "127.0.0.1:10003")

dbLoadRecords("tdkGen10500.db")
