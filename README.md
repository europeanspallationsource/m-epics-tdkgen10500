# TDK Lambda Genesys 10-500

EPICS module for the TDK Lambda Genesys 10-500 power supply.

| Property        | Value |
|-----------------|-------|
| Maximum Voltage | 10 V  |
| Maximum Current | 500 A |